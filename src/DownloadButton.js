import React, { useState, useEffect, useRef } from "react";

export default function DownloadButton() {
    const downloadUrl = "http://abg.hu/dwnld/large-imgs.zip";
    return (
        <a className="App-link" href={downloadUrl} download="big-file">
            Start to Download
        </a>
    );
}
