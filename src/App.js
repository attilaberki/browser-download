import "./App.css";
import DownloadButton from "./DownloadButton";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <DownloadButton />
            </header>
        </div>
    );
}

export default App;
